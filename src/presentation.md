<!-- $theme: gaia -->

# Electron as a Testing Framework

What it is and what it’s capable of


---

## Electron: Built from two major FOSS projects
- Chromium
	- Open source foundation of Google Chrome
	- CEF (Chromium Engine Framework), as opposed to full-blown custom fork of Chromium a la NW.js
	- V8 engine for frontend JS
- Node.js
	- Engine for backend JS
	- Based on V8
- Capabilities of both engines intermingle

---

## Electron Components
- Main process
	- Backend (Node)
	- Starting point to launch windows (render)
- Render process
	- Frontend (Chromium)
	- Access to Node/Electron APIs
- Webview
	- (technically part of render, but runs in a separate process)
	- Can preload scripts to interact with content, without affecting DOM
	- No access to Node/Electron APIs by default, except via preload

---

## Electron Components – JavaScript access/loading
- Main process (e.g. main.js)
	- Node.js/CommonJS style: ```require();```
	- ES6 style: import { something } from some_library;
- Render process (e.g. index.html)
	- ```<script>``` tag
	- Node.js/CommonJS style: ```require();```
- Webview (```<webview>``` tag, part of render process HTML)
	- Page is loaded like a web browser
	- Preload JS loads before any other content
		- Keep alive via listeners (.onload, etc.)
    - ```require()``` possible via preload

---

## Communication between Electron Components
- IPC (Interprocess Communication)
	- Main can send and receive messages to/from render and Webview
	- Render to/from main and webview
		- Webview sends message to enclosing ```<webview>``` tag in render
	- Webview to/from main and render
		- Listeners and messages set in preload script