/*
const {ipcRenderer} = require('electron');
ipcRenderer.on('testLogo', () => {
  ipcRenderer.sendToHost('testLogoWebview();');
});
*/

const fs = require('fs'),
  path = require('path'),
  notifier = require('node-notifier'),
  { ipcRenderer, screen, shell } = require('electron'),
  testLog = path.join(process.cwd(), 'results', 'test-log.txt');

window.addEventListener('DOMContentLoaded', (() => {
  //isGoogleHome(startRecording);
  const searchTest = new Promise((resolve) =>
    resolve(
      isGoogleHome(() => {
        ipcRenderer.sendToHost('video-start');
        console.log('IPC message sent: video-start.')
      })
    )
  )
  .then(() =>
    testPrep()
  )
  .then(() =>
    logoTest()
  )
  .then(() =>
    searchForMe()
  )
  .then(() =>
    goToMe()
  )
  .then(() =>
    goToPie()
  )
  .then(() =>
    isMBArticle(() => {
      shell.openExternal('file://' + testLog);
      ipcRenderer.sendToHost('video-stop');
      console.log('IPC message sent: video-stop.');
    })
  );
}));

function testPrep() {
  // First make sure we're at Google homepage
  isGoogleHome(() => {
    // Create blank logfile. Will delete previous test log if it exists.
    fs.writeFile(testLog, '', (err) => {
      if (err) {
          console.log('Error writing to' + testLog);
      }
    });

    // OS native notification (baloon in Windows 7)
    notifier.notify({
      'title': 'Electron Demo - Startup',
      'message': 'Demo page loaded in webview. DevTools will now open for the page. Watch the DevTools console for test output.'
    });
  });
}

function logoTest() {
  // Still on Google homepage?
  isGoogleHome(() =>
    // Test for Google Logo
    // Guaranteed fail for one, pass for the other on http//www.google.com.
    logoTF = new Promise((res) =>
      // (default logo in Drupal with "Stark" theme - intentionally false/FAIL)
      res(
        testSelectorExists('#block-stark-branding img', testLog)
      )
    )
    .then(() =>
    // (Selector for logo on Google.com front page)
      testSelectorExists('#hplogo', testLog)
    )
  );
}


function isGoogleHome(callback) {
  if (location.host == 'www.google.com' && location.pathname == '/') {
    callback();
  }
}

function isGoogleSerp(callback) {
  if (location.host == 'www.google.com' && location.pathname == '/search') {
    callback();
  }
}

function isMBBlog(callback) {
  if (location.host == 'mikebybee.com' && location.pathname == '/blog/') {
    callback();
  }
}

function isMBArticle(callback) {
  if (location.host == 'mikebybee.com' && location.pathname == '/blog/honey-coffee-walnut-pie') {
    callback();
  }
}

function searchForMe() {
  // Still Google homepage?
  isGoogleHome(() =>
    // Search for my blog
    googleSearch = new Promise((res) =>
      res(
        setTimeout(() => {
          document.querySelector('input#lst-ib.gsfi').value = 'mikebybee.com/blog/';
        }, 5000))
      )
      .then(() =>
      setTimeout(() => {
        document.querySelector('form#tsf').submit();
          }, 5000),
    (err) =>
      console.log('Unable to navigate page.' + err)
    )
  );
}

function goToMe() {
  // Fire only on Google search results page.
  isGoogleSerp(() =>
    // Go to SERP link to my blog's main page
    setTimeout(() => {
      document.querySelector('a[href="https://mikebybee.com/blog/"]').click();
    }, 5000)
  );
}

function goToPie() {
  // Click on "Honey Coffee Walnut Pie" recipe. You're welcome.
  isMBBlog(() =>
    setTimeout(() => {
      document.querySelector('a[href="honey-coffee-walnut-pie"]').click();
    }, 10000)
  );
}

function testSelectorExists(selector, log, seconds = 5) {
  setTimeout(() => {
    let passFail = 'FAIL',
    logMsg = 'Logging at ' + log + '.',
    selectBool = document.querySelector(selector) !== null;
    if (selectBool) {
      passFail = 'PASS';
    }

    let result = passFail + ' Found expected selector "' + selector + '": ' + selectBool + '.';

    // Log to webview's DevTools.
    console.log(result + '\n' + logMsg + '\n\n');

    // Notify via OS native notifications or Growl.
    notifier.notify({
      'title': 'Electron Demo - Selector Test',
      'message': result + ' ' + logMsg,
    });

    // Write to logfile.
    fs.appendFile(log, result + '\r\n', (err) => {
      if (err) {
        console.error('Error writing to' + log);
      }
    });
  }, seconds * 1000);
}
